# -*- coding: utf-8 -*-
"""
Created on Wed Apr  5 06:00:08 2023

@author: Sumana
"""

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sb
import numpy as np


def world_data(filedata):
    """
    Read the CSV file and extract required data.

    Parameters:
    filedata (str): Path of the CSV file.

    Returns:
    tuple: A tuple containing two pandas DataFrames - df_name and countries.
    """
    # read the CSV file and skip the first 4 rows of metadata
    dataframe = pd.read_csv(filedata, skiprows=4)
    countries = dataframe.drop(
        columns=['Country Code', 'Indicator Code', 'Unnamed: 66'], inplace=True)
    countries = dataframe.set_index('Country Name').T
    df_name = dataframe.set_index('Country Name').reset_index()
    return df_name, countries


def attribute(indicators, info):
    '''
    function for choosing an attribute
    '''
    info = info[info['Indicator Name'].isin([indicators])]
    return info


def choose_country(countries, info):
    """
    Extracts the specified country's data from the dataframe and returns it.

    Args:
    countries (str): The name of the country to extract.
    info (pd.DataFrame): The dataframe containing the data.

    Returns:
    pd.DataFrame: The extracted data.
    """
    info = info[info['Country Name'].isin([countries])]
    info = info.set_index("Indicator Name")
    info = info.drop("Country Name", axis=1)
    # Transposing the dataframe
    info = info.T
    return info


#function to plot a multiple-bar graph
def plot_indicator_for_countries(countries, indicator):
    """
    Plot the specified indicator for the given list of countries.
    """
    selected_data = df_name.loc[(df_name['Country Name'].isin(countries)) &
                                (df_name['Indicator Name'] == indicator) &
                                (df_name[['1970', '1980', '1990', '2000', '2010', '2020']].notnull().all(axis=1)), :]

    # Grouping the selected data
    selected_data_grouped = selected_data.groupby(
        'Country Name')[['1970', '1980', '1990', '2000', '2010', '2020']].agg(list)

    fig, ax = plt.subplots()

    # Initialize variables for plotting
    width = 0.13
    colors = ['#56AB91', '#70D6FF', '#FF70A6', '#FF9770', '#FFD670', '#E9FF70']
    years = ['1970', '1980', '1990', '2000', '2010', '2020']
    x_coords = np.arange(len(countries))
    x_offsets = np.arange(len(years)) - 3
    x_coords = x_coords[:, None] + x_offsets[None, :] * width
    data = [selected_data_grouped[year].apply(lambda x: x[0]) for year in years]

    # Plot the bar chart
    for i, year in enumerate(years):
        ax.bar(x_coords[:, i], data[i], width, label=year,
               color=colors[i], alpha=1, edgecolor='black')

    ax.set_xticks(x_coords.mean(axis=1))
    ax.set_xticklabels(countries, rotation=90)
    ax.set_ylabel(indicator)
    ax.set_title(indicator, fontsize="10")
    ax.legend(fontsize="7", loc="upper right")

    # Calculate the skewness of the selected data
    skew = selected_data_grouped.skew(axis=0, skipna=True)
    skew.head()

    # Display the plot
    plt.show()


#function to plot a multi-line graph
def plot_indicator(indicator, countries, df):

    years = [str(year) for year in range(1990, 2020, 5)]
    data = df.loc[df['Country Name'].isin(
        countries) & df['Indicator Name'].isin([indicator]), years].T
    kurt = data.kurtosis(axis=0, skipna=True)
    print(kurt.head())
    plt.plot(data, linestyle='-.')
    plt.legend(countries, fontsize="7", loc="upper right" )
    plt.xlabel('Year')
    plt.ylabel(indicator)
    plt.show()

    
#function to plot a heat map
def plot_heatmap(country, data, cols, cmap='viridis'):  
    subset = choose_country(country, data)
    columns = subset[cols]
    corr = columns.corr()
    sb.heatmap(corr, annot=True, cmap=cmap)
    plt.title(f"Correlation matrix for {country}")
    plt.show()

        
#reading the data 
df_name, countries = world_data(r"C:\Users\navee\Downloads\climate_change_wbdata.csv")

#describe()
print(df_name.describe())
print(countries.describe())

#seleted countries for bar graph
countries1 = ['Argentina', 'Australia', 'Brazil',
              'Canada', 'China', 'India', 'Sri Lanka']
indicator1 = 'Population growth (annual %)'
indicator2 = 'Arable land (% of land area)'
plot_indicator_for_countries(countries1, indicator1)
plot_indicator_for_countries(countries1, indicator2)

#plotting a line graph
plot_indicator('Forest area (% of land area)', [
               'Argentina', 'Australia', 'Canada', 'China', 'India', 'Sri Lanka', 'Afghanistan'], df_name)

plot_indicator('Total greenhouse gas emissions (% change from 1990)', [
               'Argentina', 'Australia', 'Canada', 'China', 'India', 'SriLanka', 'Afghanistan'], df_name)

#plotting a heat map    
plot_heatmap('Argentina', df_name, ['Electricity production from oil sources (% of total)',
                                    'Electricity production from natural gas sources (% of total)',
                                    'Electricity production from hydroelectric sources (% of total)',
                                    'Electricity production from coal sources (% of total)'], cmap='BuPu')

plot_heatmap('Canada', df_name, ['Urban population (% of total population)',
                                 'Agriculture, forestry, and fishing, value added (% of GDP)',
                                 'Total greenhouse gas emissions (% change from 1990)',
                                 'Agricultural land (% of land area)',
                                 'Population growth (annual %)'], cmap='YlGnBu')
